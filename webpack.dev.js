const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');

module.exports = merge(common, {
		mode: 'development',
		devServer: {
				host: '0.0.0.0',
				publicPath: './src',
				contentBase: path.resolve(__dirname, './src'),
				watchContentBase: true,
				writeToDisk: true,
				compress: true,
		},
});
