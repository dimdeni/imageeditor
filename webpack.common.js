const path = require('path');

const config = {
  entry: './src/js/app.js',
  output: {
    path: path.resolve(__dirname, './src'),
    filename: 'js/bundle.js'
  },
		module: {
				rules: [{
						test: /\.js$/, // files ending with .js
						exclude: /node_modules/, // exclude the node_modules directory
						loader: "babel-loader", // use this (babel-core) loader
						query: {
								presets:[
										['@babel/preset-env',
										{
												targets: {
														edge: "17",
														firefox: "60",
														chrome: "67",
														safari: "11.1",
												},
												useBuiltIns: "usage",
										},
								],
								]

						}
				}]
		}
};

module.exports = config;
