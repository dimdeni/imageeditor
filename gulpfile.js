const gulp = require('gulp');
const del = require('del');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const browsersync = require('browser-sync');
const merge = require('merge-stream');
const imagemin = require('gulp-imagemin');
const spritesmith = require('gulp.spritesmith');

function style() {
		return gulp
				.src('src/scss/**/*.scss')
				.pipe(autoprefixer({
						browsers: ['>0.1%'],
						cascade: false
				}))
				.pipe(concat('style.css'))
				.pipe(sass.sync({outputStyle: 'compressed'})
						.on('error', sass.logError))
				.pipe(gulp.dest("src/style/"))
				.pipe(browsersync.stream())

}

function browserSync(done) {
		browsersync.init({
				server: {
						baseDir: "./src"
				},
				port: 3000
		});
		done();
}

function browserSyncReload(done) {
		browsersync.reload();
		done();
}

function watchFiles() {
		gulp.watch('src/scss/**/*', style);
		gulp.watch('src/*.html', browserSyncReload);
}

function remove(){
		return del('src/style/*')
		}
gulp.task('cleanBuild', ()=>{ return del('build/')} );

function build(){
		const html = gulp
				.src(['src/*.html'])
				.pipe(gulp.dest('build/'));

		const style = gulp
				.src([
						'src/style/*'])
				.pipe(gulp.dest('build/style'));

		return merge(html, style);


}

function img(){
		return gulp
				.src('src/img/**/*.{jpg, jpeg, png, svg}')
				.pipe(imagemin([
						imagemin.jpegtran({progressive: true}),
						imagemin.optipng({optimizationLevel: 5}),
						imagemin.svgo({
								plugins: [
										{removeViewBox: true},
										{cleanupIDs: false}
								]
						})
				]))
				.pipe(gulp.dest("build/img"))

}

function sprite(){
		return gulp.src('src/icon/*.png')
				.pipe(spritesmith({
				imgName: 'sprite.png',
				cssName: 'sprite.css'
		}))
				.pipe(gulp.dest("build/icon"));
}

gulp.task('style', gulp.series(remove, style));

gulp.task('watch', gulp.parallel(watchFiles, browserSync), style);

gulp.task('build', gulp.series('cleanBuild', build, img));

gulp.task('img', img);

gulp.task('sprite', sprite);