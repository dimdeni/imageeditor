import {showErrorMessage} from "./error";
import saveAs from "file-saver";




class Upload extends Image {

		constructor(){
				super();
				this.allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
				this.inputElement = document.querySelector('.content__input');
				this.reader = new FileReader();
				this.image = document.createElement('img');
				this.canvas = document.querySelector('.content__canvas');
		}


		loadImageReader(){




				this.inputElement.addEventListener('change', (evt)=>{
						this.file = evt.target.files[0];

						if (!this.allowedExtensions.exec(this.file.name)){

								return showErrorMessage('Error: allowed file extension: "png", "jpg"');

						}


						this.reader.addEventListener('load', ()=>{

								this.loadImg();

								this.save();


						});

						const readImg = this.reader.readAsDataURL(this.file);


				});

		}

		renderCanvas(){
				this.canvas.width = this.image.width;
				this.canvas.height = this.image.height;


				let ctx = this.canvas.getContext('2d');


				ctx.drawImage(this.image, 0, 0);

		}

		loadImg(){
				this.image.addEventListener('load', ()=>{

						this.renderCanvas();

				});

				this.image.src = this.reader.result;
		}

		save(){
				const saveButton = document.querySelector('.content__save');
				saveButton.addEventListener('click', (evt)=>{
						if (evt.target===saveButton){
								this.canvas.toBlob(function(blob) {
										saveAs(blob, "image.png");

								});
						}
				});
		}
}



export {Upload};
