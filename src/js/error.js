const content = document.querySelector('.content');
const span = document.createElement('span');

const showErrorMessage = (message)=>{
		span.classList.add('error');
		const spanElement = content.appendChild(span);
		spanElement.textContent = message;

	spanElement.addEventListener('click', (evt)=>{
				if (evt.target===spanElement){
						spanElement.remove();
				}

		});
		setTimeout(()=>{
				spanElement.remove();
		}, 5000)
};


export {showErrorMessage};